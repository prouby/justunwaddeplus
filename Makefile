# Makefile

WADS_DIR=wads

WAD_INFO=wadinfo.txt

DEUTEX=deutex
DEUTEX_ARGS=-v0 -rate accept -rgb 0 47 47
DEUTEX_INCLUDES=-lumps -patch -flats -sounds -musics -graphics -sprites -levels

WAD=$(WADS_DIR)/justunwaddeplus.wad

all: $(WAD)

# Wad
$(WAD): $(WAD_INFO)
	@echo "  WAD   $@"
	@mkdir -p $(WADS_DIR)
	@rm -f $@
	@$(DEUTEX) $(DEUTEX_ARGS) -iwad $(DEUTEX_INCLUDES) -build $(WAD_INFO) $@

check: $(WAD)
	@echo "  CHK   $<"
	@$(DEUTEX) $(DEUTEX_ARGS) -check $<

# Clean
.PHONY: clean clean_wads
clean: clean_wads

clean_wads:
	@echo "  CLN   $(WAD)"
	@rm -f $(WAD)
